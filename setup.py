from distutils.core import setup
from setuptools import find_packages

# need this to be able to install from local directory
setup(
    name="clippy",
    packages=find_packages(),
)
