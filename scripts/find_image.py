# screenshot = "data/tasks/current/81a6c97b-0d1e-5e3a-855e-2902436dfde0.png"
# elem = "elem1.png"

import json

data_dir = "data/tasks/current"
task_data = f"{data_dir}/task.json"

with open(task_data, "r") as f:
    task = json.load(f)

ste = task["steps"][1]

screenshot = data_dir + f"/{ste['id']}.png"

bounding_box = ste["actions"][1]["bounding_box"]


# import cv2 as cv
import cv2
import numpy as np
from matplotlib import pyplot as plt

# DRAW A BOUNDING BOX ON THE SCREENSHOT

img = cv2.imread(screenshot)

outfile = "test.png"

# x1, y1, x2, y2 = bounding_box['x'], bounding_box['y'], bounding_box['x'] + bounding_box['width'], bounding_box['y'] + bounding_box['height']
x1, y1, x2, y2 = (
    bounding_box["x"],
    bounding_box["y"] + bounding_box["scrollY"],
    bounding_box["x"] + bounding_box["scrollX"] + bounding_box["width"],
    bounding_box["y"] + bounding_box["scrollY"] + bounding_box["height"],
)

extra_buffer = 20
x1, x2, y1, y2 = int(x1), int(x2), int(y1), int(y2)
x1, x2, y1, y2 = x1 - extra_buffer, x2 + extra_buffer, y1 - extra_buffer, y2 + extra_buffer

cv2.rectangle(img, (x1, y1), (x2, y2), (0, 255, 0), 4)
cv2.imwrite(outfile, img)


templated_img = img[y1:y2, x1:x2]
# save small template image

cv2.imwrite("template.png", templated_img)

# breakpoint()


# THIS IS FOR FINDING THE IMAGE IN THE SCREENSHOT
# elem, screenshot = screenshot, elem
# image = cv2.imread(elem)
# template = cv2.imread(screenshot)
template = cv2.imread(screenshot)
elem = cv2.imread("template.png")

# elem, template = template, elem

# # convert both the image and template to grayscale
imageGray = cv2.cvtColor(elem, cv2.COLOR_BGR2GRAY)
templateGray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)


# print("[INFO] performing template matching...")
result = cv2.matchTemplate(imageGray, templateGray, cv2.TM_CCOEFF_NORMED)
(minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(result)

(startX, startY) = maxLoc
endX = startX + elem.shape[1]
endY = startY + elem.shape[0]
# breakpoint()


cv2.rectangle(template, (startX, startY), (endX, endY), (255, 0, 0), 3)

middle_point = (startX + endX) // 2, (startY + endY) // 2
print("middle point for template: ", middle_point)
cv2.circle(template, middle_point, 5, (0, 0, 255), -1)
# draw middle point as red dot

cv2.imwrite("template-from.png", template)

# # show the output image
# cv2.imshow("Output", image)
# cv2.waitKey(0)
