import uuid
from typing import Any, List, Tuple

from datetime import datetime
from clippy.crawler.states.actions import Action
from playwright.async_api import ConsoleMessage, Frame, Page
from playwright.sync_api import Frame as FrameSync
from playwright.sync_api import Page as PageSync

short_url_n = 100  # if None will print whole


# im using id which is a reserved word but without that
class Step:
    def __init__(self, id: str = None, url: str = None, **kwargs):
        if id is None:
            id = str(uuid.uuid5(uuid.NAMESPACE_URL, url))

        self.id = id
        self.url = url
        self.actions: List[Action] = []

    def __call__(self, action: Action, **kwargs):
        self.actions.append(action)

    def __repr__(self):
        actions = "\n".join(["\t\t" + "-" + str(a) for a in self.actions])
        return f"\tPage: {self.url}\n{actions}"

    @classmethod
    def from_dict(cls, data: dict):
        if len(data["actions"]) == 0:
            return None

        actions = data.pop("actions")
        step = cls(**data)

        for action_dict in actions:
            ActType = Action[action_dict.pop("type")]
            action = ActType(**action_dict)
            step.actions.append(action)
        return step

    @property
    def n_actions(self):
        return len(self.actions)

    def merge(self):
        if len(self.actions) < 2:
            return

        merged_actions = [self.actions.pop(0)]

        for idx, action in enumerate(self.actions):
            if merged_actions[-1].should_merge(action):
                merged_actions[-1].update(action)
            else:
                merged_actions.append(action)

        self.actions = merged_actions

    def format_url(self):
        url = self.url or self.id
        if (short_url_n is not None) and (len(url) > short_url_n):
            url = self.url[:short_url_n] + "..."
        return url

    def print(self):
        url = self.format_url()

        print(f"\n===\nprinting step:{url}")
        for action in self.actions:
            print(action)

    def as_dict(self):
        # _url = str(uuid.uuid5(uuid.NAMESPACE_URL, self.url))
        return {
            # "page": _url,  # should be screenshot in future
            "url": self.url,
            "id": self.id,
            "actions": [a.as_dict() for a in self.actions],
        }


class Task:
    def __init__(self, objective: str, id: str = None, timestamp: str = None):
        self.objective = objective
        self.id = str(uuid.uuid4()) if id is None else id

        self.steps: List[Step] = []
        self.curr_step = None

        if timestamp is None:
            timestamp = str(datetime.now())

        self.timestamp = timestamp

    def __call__(self, action: Action, **kwargs):
        if not isinstance(action, Action):
            raise ValueError(f"action must be of type Action, not {type(action)}")

        self.curr_step(action, **kwargs)

    def __repr__(self):
        steps_info = "\n".join([f"{s}" for s in self.steps])
        return f"Task: {self.objective} | {len(self.steps)} steps \n{steps_info}"

    @classmethod
    def from_dict(cls, data: dict):
        id = data.get("id", None)
        objective = data.get("objective", None)
        timestamp = data.get("timestamp", None)

        steps = data.pop("steps", [])

        task = cls(**data)
        for step_dict in steps:
            step = Step.from_dict(step_dict)

            if step is None:
                continue

            task.steps.append(step)
        return task

    @property
    def n_steps(self):
        return len(self.steps)

    @property
    def n_actions(self):
        return sum([step.n_actions for step in self.steps])

    def print(self):
        return self.__repr__()

    async def page_change_async(self, page: Page | Frame | str):
        self.page_change(page=page)

    def page_change(self, page: Page | Frame | str):
        if page is None:
            breakpoint()
        url = page
        if isinstance(page, (Page, Frame, PageSync, FrameSync)):
            url = page.url

        if self._check_new_page(url) is False:
            return
        self.curr_step = Step(url=url)
        self.steps.append(self.curr_step)

    def _check_new_page(self, url: str):
        if self.curr_step:
            if self.curr_step.url == url:
                return False
            if self.curr_step.url is None:
                self.curr_step.url = url
                return False

        if (prev_step := self.curr_step) is not None:
            prev_step.merge()

        return True

    def dump(self):
        # dump as json)
        data = {
            "id": self.id,
            "timestamp": self.timestamp,
            "objective": self.objective,
            "steps": [step.as_dict() for step in self.steps],
        }
        return data
