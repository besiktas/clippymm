from dataclasses import dataclass
from typing import List
import tinydb

from sqlalchemy import text
from sqlalchemy_utils import create_database, database_exists
from sqlmodel import JSON, Field, MetaData, Session, SQLModel, create_engine, text
import sqlalchemy


@dataclass
class DatabaseConfig:
    url: str = "postgresql+psycopg2://localhost"
    database_name: str = "clippy"


class DatabaseSetup:
    """
    this class is a bit unnecessary but i need a stub for testing
    """

    def __init__(
        self,
        config: DatabaseConfig = DatabaseConfig(),
        echo: bool = True,
    ):
        url = f"{config.url}/{config.database_name}"

        if not database_exists(url):
            create_database(url)

        self.engine = create_engine(url=url, echo=echo)
        self.metadata_obj = MetaData()

    def setup(self):
        # install the extension for vectors
        with self.engine.connect() as conn:
            conn.execute(text("CREATE EXTENSION IF NOT EXISTS vector"))
            conn.commit()

        # drop and recreate the tables
        SQLModel.metadata.drop_all(self.engine)
        SQLModel.metadata.create_all(self.engine)
        return self.engine

    def add(self, *items: SQLModel):
        with Session(self.engine) as session:
            for item in items:
                session.add(item)
            session.commit()

    def make_table(self, table_name: str = "testdb") -> None:
        with sqlalchemy.create_engine("postgresql://postgres@localhost:5432", isolation_level="AUTOCOMMIT").connect() as connection:
            connection.execute(f"CREATE DATABASE {table_name}")


class ResponseData:
    def __init__(self, datapath: str = "data/tdb/db.json"):
        self.db = tinydb.TinyDB(datapath)
